function convertToBinary(number, base) {

    if (base === 2) {
        return number.toString(2)
    }

}

const numberToConvert = convertToBinary(104, 2)

console.log(numberToConvert);