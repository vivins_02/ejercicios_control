const axios = require('axios');

async function getData() {
    const URL_page1 = `https://rickandmortyapi.com/api/episode`;
    const URL_page2 = `https://rickandmortyapi.com/api/episode?page=2`;
    const URL_page3 = `https://rickandmortyapi.com/api/episode?page=3`;

    const data_page1 = await axios.get(URL_page1);
    const data_page2 = await axios.get(URL_page2);
    const data_page3 = await axios.get(URL_page3);

    const results = Promise.all([data_page1, data_page2, data_page3]);

    const dataResultsPage1 = data_page1.data.results;
    const dataResultsPage2 = data_page2.data.results;
    const dataResultsPage3 = data_page3.data.results;

    const allResults = [
        ...dataResultsPage1,
        ...dataResultsPage2,
        ...dataResultsPage3
    ];

    let episodeCollect = []

    allResults.forEach(episode => {
        for (i = 0; i < allResults.length; i++) {
            episodeData = new Object();
            episodeData.airDate = allResults[i].air_date;
            episodeData.episode = allResults[i].episode;
            episodeData.characters = allResults[i].characters;

            episodeCollect.push(episodeData)
        }
    });

    let foundJanuary = episodeCollect.filter(item => item.airDate.includes('January'))

    let urlCharacters = []
    for (let i = 0; i < foundJanuary.length; i++) {
        for (let u = 0; u < foundJanuary[i].characters.length; u++) {
            // console.log(foundJanuary[i].characters[u]);
            urlCharacters.push(foundJanuary[i].characters[u])
        }
    }

    const uniqueUrls = urlCharacters.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    })

    let dataCharacters = []
    for (let i = 0; i < uniqueUrls.length; i++) {
        const dataResultCharacters = (await axios.get(uniqueUrls[i]))
        dataCharacters.push(dataResultCharacters.data.name)
    }

    console.log(dataCharacters);
}


const resultado = getData()