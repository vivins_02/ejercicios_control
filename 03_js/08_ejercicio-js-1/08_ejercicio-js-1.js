const axios = require('axios');

async function getData(numberResults) {
    const URL = `https://randomuser.me/api/?results=${numberResults}&inc=login,name,gender,nat,name,email,picture&noinfo`;

    const datos = await axios.get(URL)

    const results = datos.data.results

    let resultsList = []

    for (i = 0; i < results.length; i++) {
        resultsObj = new Object();
        resultsObj.username = results[i].login.username;
        resultsObj.name = `${results[i].name.first} ${results[i].name.last}`;
        resultsObj.gender = results[i].gender;
        resultsObj.nationality = results[i].nat;
        resultsObj.email = results[i].email;
        resultsObj.picture = results[i].picture.medium;

        resultsList.push(resultsObj)
    };

    console.log(resultsList);
}

const resultado = getData(3)