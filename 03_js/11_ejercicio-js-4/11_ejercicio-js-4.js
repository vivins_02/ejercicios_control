const names = [
    'A-Jay',
    'Manuel',
    'Maria',
    'Manuel',
    'manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'arun',
    'Kenton',
];

namesLower = names.map(function (name) {
    return name.toLowerCase()
})

const unique = namesLower.filter(function (value, index, self) {
    return self.indexOf(value) === index;
})

const namesCapitalized = unique.map(function (name) {
    return name.replace(/^\w/, (c) => c.toUpperCase());
});


console.log(namesCapitalized);