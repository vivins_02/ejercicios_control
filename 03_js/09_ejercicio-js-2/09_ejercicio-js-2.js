const presentTime = Date.now();
const nowTime = new Date(presentTime)

const execution = () => {
    const timeTransgressed = parseInt((Date.now() - nowTime.getTime()) / 1000);

    const days = Math.floor(timeTransgressed / 60 / 60 / 24);
    const hours = Math.floor(timeTransgressed / 60 / 60 % 24);
    const minutes = Math.floor(timeTransgressed / 60 % 60);
    const seconds = Math.floor(timeTransgressed % 60);

    console.log(`Time since execution: ${seconds} seconds, ${minutes} minutes, ${hours} hours, ${days} days`);
}

const interval = setInterval(execution, 5000);

console.log(`Execution Time: ${nowTime.toLocaleTimeString()}`);